package com.quyetdv7.domain.repository

import com.quyetdv7.domain.Resource
import com.quyetdv7.domain.model.castandcrew.CastAndCrew
import com.quyetdv7.domain.model.detail.MovieDetail
import com.quyetdv7.domain.model.list.MovieListPage

interface MovieRepository {
    suspend fun getMovieDetails(movieId: Int): Resource<MovieDetail>
    suspend fun getCastAndCrewList(movieId: Int): Resource<CastAndCrew>
    suspend fun getPopularMovieList(pageNumber: Int): Resource<MovieListPage>
    suspend fun getTopRatedMovieList(pageNumber: Int): Resource<MovieListPage>
    suspend fun getUpComingMovieList(pageNumber: Int): Resource<MovieListPage>
    suspend fun getNowPlayingMovieList(pageNumber: Int):Resource<MovieListPage>
}