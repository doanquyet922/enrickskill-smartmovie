package com.quyetdv7.domain.repository

import kotlinx.coroutines.flow.Flow
import com.quyetdv7.domain.model.favorite.FavoriteMovie

interface FavoriteMovieRepository {
    suspend fun getFavoriteMovieList(): Flow<List<FavoriteMovie>>
    suspend fun insertFavoriteMovie(movie: FavoriteMovie)
}