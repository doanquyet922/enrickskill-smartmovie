package com.quyetdv7.domain.repository

import com.quyetdv7.domain.Resource
import com.quyetdv7.domain.model.list.MovieListPage

interface SearchRepository {
    suspend fun getSearchResultList(pageNumber: Int, query: String): Resource<MovieListPage>
}