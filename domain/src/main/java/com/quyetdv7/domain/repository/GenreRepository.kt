package com.quyetdv7.domain.repository

import com.quyetdv7.domain.Resource
import com.quyetdv7.domain.model.detail.Genre
import com.quyetdv7.domain.model.list.MovieListPage

interface GenreRepository {
    suspend fun getGenreList(): Resource<List<Genre>>
    suspend fun getMovieListByGenre(genreId: Int, pageNumber: Int): Resource<MovieListPage>
}