package com.quyetdv7.domain.usecase

import com.quyetdv7.domain.Resource
import com.quyetdv7.domain.model.list.MovieListPage
import com.quyetdv7.domain.repository.MovieRepository
import javax.inject.Inject

class GetTopRatedMovieListPageUseCase @Inject constructor(
    private val movieRepository: MovieRepository
) {
    suspend fun execute(page: Int): Resource<MovieListPage> {
        return movieRepository.getTopRatedMovieList(page)
    }
}