package com.quyetdv7.domain.usecase

import kotlinx.coroutines.flow.Flow
import com.quyetdv7.domain.model.favorite.FavoriteMovie
import com.quyetdv7.domain.repository.FavoriteMovieRepository
import javax.inject.Inject

class GetFavoriteMovieListUseCase @Inject constructor(
    private val favoriteMovieRepository: FavoriteMovieRepository
) {
    suspend fun execute(): Flow<List<FavoriteMovie>> {
        return favoriteMovieRepository.getFavoriteMovieList()
    }
}