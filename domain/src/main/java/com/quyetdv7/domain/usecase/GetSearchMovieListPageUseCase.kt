package com.quyetdv7.domain.usecase

import com.quyetdv7.domain.Resource
import com.quyetdv7.domain.model.list.MovieListPage
import com.quyetdv7.domain.repository.SearchRepository
import javax.inject.Inject

class GetSearchMovieListPageUseCase @Inject constructor(
    private val searchRepository: SearchRepository
) {
    suspend fun execute(query: String, page: Int): Resource<MovieListPage> {
        return searchRepository.getSearchResultList(page, query)
    }
}