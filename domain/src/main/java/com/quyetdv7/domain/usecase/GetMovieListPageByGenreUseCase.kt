package com.quyetdv7.domain.usecase

import com.quyetdv7.domain.Resource
import com.quyetdv7.domain.model.list.MovieListPage
import com.quyetdv7.domain.repository.GenreRepository
import javax.inject.Inject

class GetMovieListPageByGenreUseCase @Inject constructor(
    private val genreRepository: GenreRepository
) {
    suspend fun execute(genreId: Int, page: Int): Resource<MovieListPage> {
        return genreRepository.getMovieListByGenre(genreId, page)
    }
}