package com.quyetdv7.domain.usecase

import com.quyetdv7.domain.Resource
import com.quyetdv7.domain.model.detail.MovieDetail
import com.quyetdv7.domain.repository.MovieRepository
import javax.inject.Inject

class GetMovieDetailUseCase @Inject constructor(
    private val movieRepository: MovieRepository
) {
    suspend fun execute(movieId: Int): Resource<MovieDetail> {
        return movieRepository.getMovieDetails(movieId)
    }
}