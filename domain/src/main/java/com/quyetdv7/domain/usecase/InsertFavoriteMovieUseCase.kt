package com.quyetdv7.domain.usecase

import com.quyetdv7.domain.model.favorite.FavoriteMovie
import com.quyetdv7.domain.repository.FavoriteMovieRepository
import javax.inject.Inject

class InsertFavoriteMovieUseCase @Inject constructor(
    private val favoriteMovieRepository: FavoriteMovieRepository
) {
    suspend fun execute(favoriteMovie: FavoriteMovie){
        return favoriteMovieRepository.insertFavoriteMovie(favoriteMovie)
    }
}