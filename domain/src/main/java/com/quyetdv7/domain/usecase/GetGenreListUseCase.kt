package com.quyetdv7.domain.usecase

import com.quyetdv7.domain.Resource
import com.quyetdv7.domain.model.detail.Genre
import com.quyetdv7.domain.repository.GenreRepository
import javax.inject.Inject

class GetGenreListUseCase @Inject constructor(
    private val genreRepository: GenreRepository
) {
    suspend fun execute(): Resource<List<Genre>> {
        return genreRepository.getGenreList()
    }
}