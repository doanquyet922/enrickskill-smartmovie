package com.quyetdv7.domain.usecase

import com.quyetdv7.domain.Resource
import com.quyetdv7.domain.model.castandcrew.CastAndCrew
import com.quyetdv7.domain.repository.MovieRepository
import javax.inject.Inject

class GetCastAndCrewListUseCase @Inject constructor(
    private val movieRepository: MovieRepository
) {
    suspend fun execute(movieId: Int): Resource<CastAndCrew> {
        return movieRepository.getCastAndCrewList(movieId)
    }
}