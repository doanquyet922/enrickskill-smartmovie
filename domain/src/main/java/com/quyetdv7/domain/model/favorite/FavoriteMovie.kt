package com.quyetdv7.domain.model.favorite

data class FavoriteMovie(val movieId: Int, val isLiked: Boolean)