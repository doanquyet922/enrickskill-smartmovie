package com.quyetdv7.domain.model.castandcrew

data class CastAndCrew(
    val casts: List<Cast>,
    val crews: List<Crew>,
    val id: Int
)