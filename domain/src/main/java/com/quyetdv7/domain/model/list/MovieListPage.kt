package com.quyetdv7.domain.model.list

data class MovieListPage(
    val page: Int,
    val results: List<Movie>,
    val totalPages: Int,
    val totalResults: Int
)