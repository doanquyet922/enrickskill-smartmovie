package com.quyetdv7.domain.model.detail

data class ProductionCountry(
    val name: String,
    val iso_3166_1: String
)