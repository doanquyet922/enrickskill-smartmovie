package com.quyetdv7.domain.model.detail

data class SpokenLanguage(
    val englishName: String,
    val name: String
)