package com.quyetdv7.domain.model.detail

data class MovieDetail(
    val genres: List<Genre>,
    val id: Int,
    val overview: String,
    val posterPath: String,
    val productionCountries: List<ProductionCountry>,
    val releaseDate: String,
    val runtime: Int,
    val spokenLanguages: List<SpokenLanguage>,
    val title: String,
    val voteAverage: Double
)