package com.quyetdv7.domain.model.castandcrew

data class Cast(
    val castId: Int,
    val id: Int,
    val name: String,
    val profilePath: String
)