package com.quyetdv7.domain.model.castandcrew

data class Crew(
    val id: Int,
    val name: String,
    val profilePath: String
)