package com.quyetdv7.data.repository

import kotlinx.coroutines.withContext
import com.quyetdv7.data.mapper.MovieListPageMapper
import com.quyetdv7.data.source.remote.SearchApi
import com.quyetdv7.data.utils.Constants.NETWORK_ERROR_MESSAGE
import com.quyetdv7.data.utils.DispatcherProvider
import com.quyetdv7.domain.Resource
import com.quyetdv7.domain.model.list.MovieListPage
import com.quyetdv7.domain.repository.SearchRepository
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException

class SearchRepositoryImpl(
    private var searchApi: SearchApi,
    private var dispatcherProvider: DispatcherProvider,
    private val movieListPageMapper: MovieListPageMapper
) : SearchRepository {

    override suspend fun getSearchResultList(
        pageNumber: Int,
        query: String
    ): Resource<MovieListPage> {
        return withContext(dispatcherProvider.io) {
            try {
                val movieList = searchApi.getSearchResultList(pageNumber, query)
                Resource.Success(movieListPageMapper.map(movieList))
            } catch (e: IOException) {
                Resource.Error(NETWORK_ERROR_MESSAGE)
            } catch (e: HttpException) {
                Resource.Error(NETWORK_ERROR_MESSAGE)
            } catch (e: SocketTimeoutException) {
                Resource.Error(NETWORK_ERROR_MESSAGE)
            }
        }
    }

}