package com.quyetdv7.data.repository

import com.quyetdv7.domain.repository.FavoriteMovieRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import com.quyetdv7.data.mapper.FavoriteMovieMapper
import com.quyetdv7.data.source.local.FavoriteMovieDao
import com.quyetdv7.data.utils.DispatcherProvider
import com.quyetdv7.domain.model.favorite.FavoriteMovie

class FavoriteMovieRepositoryImpl(
    private val favoriteMovieDao: FavoriteMovieDao,
    private val dispatcherProvider: DispatcherProvider,
    private val favoriteMovieMapper: FavoriteMovieMapper,
) : FavoriteMovieRepository {

    override suspend fun getFavoriteMovieList(): Flow<List<FavoriteMovie>> {
        return withContext(dispatcherProvider.io) {
            favoriteMovieDao.getFavoriteMovie().map { list ->
                favoriteMovieMapper.map(list)
            }
        }
    }

    override suspend fun insertFavoriteMovie(movie: FavoriteMovie) {
        withContext(dispatcherProvider.io) {
            favoriteMovieDao.insertMovie(favoriteMovieMapper.mapToResponse(movie))
        }
    }

}