package com.quyetdv7.data.repository

import kotlinx.coroutines.withContext
import com.quyetdv7.data.mapper.GenreMapper
import com.quyetdv7.data.mapper.MovieListPageMapper
import com.quyetdv7.data.source.remote.DiscoverApi
import com.quyetdv7.data.source.remote.GenreApi
import com.quyetdv7.data.utils.Constants.NETWORK_ERROR_MESSAGE
import com.quyetdv7.data.utils.DispatcherProvider
import com.quyetdv7.domain.Resource
import com.quyetdv7.domain.model.detail.Genre
import com.quyetdv7.domain.model.list.MovieListPage
import com.quyetdv7.domain.repository.GenreRepository
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException

class GenreRepositoryImpl(
    private val genreApi: GenreApi,
    private val discoverApi: DiscoverApi,
    private val dispatcherProvider: DispatcherProvider,
    private val genreMapper: GenreMapper,
    private val movieListPageMapper: MovieListPageMapper
) : GenreRepository {

    override suspend fun getGenreList(): Resource<List<Genre>> {
        return withContext(dispatcherProvider.io) {
            try {
                val genreList = genreApi.getGenreList()
                Resource.Success(genreMapper.map(genreList.genres ?: listOf()))
            } catch (e: IOException) {
                Resource.Error(NETWORK_ERROR_MESSAGE)
            } catch (e: HttpException) {
                Resource.Error(NETWORK_ERROR_MESSAGE)
            } catch (e: SocketTimeoutException) {
                Resource.Error(NETWORK_ERROR_MESSAGE)
            }
        }
    }

    override suspend fun getMovieListByGenre(
        genreId: Int,
        pageNumber: Int
    ): Resource<MovieListPage> {
        return withContext(dispatcherProvider.io) {
            try {
                val movieListPage = discoverApi.getMovieListByGenre(genreId, pageNumber)
                Resource.Success(movieListPageMapper.map(movieListPage))
            } catch (e: IOException) {
                Resource.Error(NETWORK_ERROR_MESSAGE)
            } catch (e: HttpException) {
                Resource.Error(NETWORK_ERROR_MESSAGE)
            } catch (e: SocketTimeoutException) {
                Resource.Error(NETWORK_ERROR_MESSAGE)
            }
        }
    }

}