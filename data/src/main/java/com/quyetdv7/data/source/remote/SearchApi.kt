package com.quyetdv7.data.source.remote

import com.quyetdv7.data.BuildConfig
import com.quyetdv7.data.response.list.MovieListPageResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchApi {

    @GET("movie?api_key=${BuildConfig.MOVIE_DB_ACCESS_KEY}&query=abc")
    suspend fun getSearchResultList(
        @Query("page") page: Int,
        @Query("query") query: String
    ): MovieListPageResponse

}