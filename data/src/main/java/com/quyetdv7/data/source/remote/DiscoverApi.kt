package com.quyetdv7.data.source.remote

import com.quyetdv7.data.BuildConfig
import com.quyetdv7.data.response.list.MovieListPageResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface DiscoverApi {

    @GET("movie?api_key=${BuildConfig.MOVIE_DB_ACCESS_KEY}")
    suspend fun getMovieListByGenre(
        @Query("with_genres") genreId: Int,
        @Query("page") page: Int
    ): MovieListPageResponse

}