package com.quyetdv7.data.source.remote

import com.quyetdv7.data.response.genre.GenresResponse
import com.quyetdv7.data.BuildConfig
import retrofit2.http.GET

interface GenreApi {

    @GET("movie/list?api_key=${BuildConfig.MOVIE_DB_ACCESS_KEY}")
    suspend fun getGenreList(): GenresResponse

}