package com.quyetdv7.data.mapper

import com.quyetdv7.data.response.detail.GenreResponse
import com.quyetdv7.data.utils.Constants.EMPTY_TEXT
import com.quyetdv7.domain.model.detail.Genre
import javax.inject.Inject

class GenreMapper @Inject constructor() : Mapper<GenreResponse, Genre>() {

    override fun map(response: GenreResponse): Genre {
        return Genre(
            id = response.id ?: 0,
            name = response.name ?: EMPTY_TEXT,
            backdropPath = null
        )
    }

    override fun map(response: List<GenreResponse>): List<Genre> {
        return response.map { item -> map(item) }
    }

}