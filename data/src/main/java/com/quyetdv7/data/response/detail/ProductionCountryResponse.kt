package com.quyetdv7.data.response.detail

import com.google.gson.annotations.SerializedName

class ProductionCountryResponse(
    @field:SerializedName("iso_3166_1") val iso_3166_1: String?,
    @field:SerializedName("name") val name: String?
)