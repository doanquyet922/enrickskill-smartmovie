package com.quyetdv7.data.response.castandcrew

import com.google.gson.annotations.SerializedName

class CastAndCrewResponse(
    @field:SerializedName("id") val id: Int?,
    @field:SerializedName("cast") val cast: List<CastResponse>?,
    @field:SerializedName("crew") val crew: List<CrewResponse>?
)