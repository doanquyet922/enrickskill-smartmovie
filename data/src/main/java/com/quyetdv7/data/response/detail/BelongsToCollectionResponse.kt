package com.quyetdv7.data.response.detail

import com.google.gson.annotations.SerializedName

data class BelongsToCollectionResponse(
    @field:SerializedName("backdrop_path") val backdropPath: String?,
    @field:SerializedName("id") val id: Int?,
    @field:SerializedName("name") val name: String?,
    @field:SerializedName("poster_path") val posterPath: String?
)
