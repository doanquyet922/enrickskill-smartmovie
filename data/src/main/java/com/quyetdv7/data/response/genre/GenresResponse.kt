package com.quyetdv7.data.response.genre

import com.google.gson.annotations.SerializedName
import com.quyetdv7.data.response.detail.GenreResponse

data class GenresResponse(
    @field:SerializedName("genres") val genres: List<GenreResponse>?
)