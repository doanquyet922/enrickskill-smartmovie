package com.quyetdv7.data.response.detail

import com.google.gson.annotations.SerializedName

class ProductionCompanyResponse (
    @field:SerializedName("id") val id: Int?,
    @field:SerializedName("logo_path") val logoPath: Any?,
    @field:SerializedName("name") val name: String?,
    @field:SerializedName("origin_country") val originCountry: String?
)