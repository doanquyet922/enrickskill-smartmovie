package com.quyetdv7.data.response.list

import com.google.gson.annotations.SerializedName

data class MovieListPageResponse(
    @field:SerializedName("page") val page: Int?,
    @field:SerializedName("results") val results: List<MovieResponse>?,
    @field:SerializedName("totalPages") val totalPages: Int?,
    @field:SerializedName("totalResults") val totalResults: Int?
)
