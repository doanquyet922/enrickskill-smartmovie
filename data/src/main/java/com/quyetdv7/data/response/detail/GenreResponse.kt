package com.quyetdv7.data.response.detail

import com.google.gson.annotations.SerializedName

data class GenreResponse(
    @field:SerializedName("id") val id: Int?,
    @field:SerializedName("name") val name: String?
)