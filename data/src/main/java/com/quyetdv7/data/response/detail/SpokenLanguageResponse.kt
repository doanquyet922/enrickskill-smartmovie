package com.quyetdv7.data.response.detail

import com.google.gson.annotations.SerializedName

class SpokenLanguageResponse(
    @field:SerializedName("english_name") val englishName: String?,
    @field:SerializedName("iso_639_1") val iso_639_1: String?,
    @field:SerializedName("name") val name: String?
)