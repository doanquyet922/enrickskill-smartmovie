package com.quyetdv7.smartmovie.utils

enum class MovieItemDisplayType {
    GRID,
    VERTICAL_LINEAR
}