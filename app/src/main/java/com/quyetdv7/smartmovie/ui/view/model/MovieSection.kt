package com.quyetdv7.smartmovie.ui.view.model

import com.quyetdv7.domain.model.list.Movie
import com.quyetdv7.smartmovie.utils.MovieCategory

data class MovieSection(val sectionType: MovieCategory, val movieList: List<Movie>)
