package com.quyetdv7.smartmovie.ui.viewstate

import com.quyetdv7.domain.model.castandcrew.Cast
import com.quyetdv7.domain.model.detail.MovieDetail

data class MovieDetailViewState(
    val movieDetail: MovieDetail?,
    val casts: List<Cast>,
    val isMovieInfoLoading: Boolean,
    val isCastLoading: Boolean,
    val isError: Boolean
)