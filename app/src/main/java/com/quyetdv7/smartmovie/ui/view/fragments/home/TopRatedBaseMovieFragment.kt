package com.quyetdv7.smartmovie.ui.view.fragments.home

import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.quyetdv7.smartmovie.ui.base.fragment.BaseMovieListFragment
import com.quyetdv7.domain.model.list.MovieListPage
import com.quyetdv7.smartmovie.R
import com.quyetdv7.smartmovie.ui.base.viewmodel.ViewModelFactory
import com.quyetdv7.smartmovie.ui.viewmodel.home.HomeViewModel
import com.quyetdv7.smartmovie.ui.viewmodel.home.TopRatedListViewModel
import com.quyetdv7.smartmovie.utils.MovieCategory
import javax.inject.Inject

class TopRatedBaseMovieFragment : BaseMovieListFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private val myHomeViewModel: HomeViewModel by lazy {
        ViewModelProvider(requireParentFragment(), viewModelFactory)[HomeViewModel::class.java]
    }

    @Inject
    lateinit var topRatedViewModel: TopRatedListViewModel

    override fun getLayoutID(): Int {
        return R.layout.fragment_top_rated_movie
    }

    override fun initInjection() {
        fragmentComponent.inject(this)
    }

    override fun initViewModel() {
        homeViewModel = myHomeViewModel
        movieListViewModel = topRatedViewModel
    }

    override fun getFirstMovieListPage(): MovieListPage? {
        return homeViewModel.currentState.movieSectionMap[MovieCategory.TOP_RATED]?.data
    }

    override fun goToMovieDetailPage(movieId: Int) {
        val action = HomeFragmentDirections.actionHomeFragmentToMovieDetailFragment2(movieId)
        findNavController().navigate(action)
    }

}