package com.quyetdv7.smartmovie.ui.viewmodel.moviebygenre

import com.quyetdv7.domain.Resource
import com.quyetdv7.domain.model.detail.MovieDetail
import com.quyetdv7.domain.model.list.MovieListPage
import com.quyetdv7.domain.usecase.GetMovieDetailUseCase
import com.quyetdv7.domain.usecase.GetMovieListPageByGenreUseCase
import com.quyetdv7.smartmovie.ui.base.viewmodel.BaseMovieListViewModel
import javax.inject.Inject

class MovieByGenreViewModel @Inject constructor(
    private val getMovieListPageByGenreUseCase: GetMovieListPageByGenreUseCase,
    private val getMovieDetailUseCase: GetMovieDetailUseCase
) : BaseMovieListViewModel() {

    private var genreId = 0

    override suspend fun getMovieListPage(page: Int): Resource<MovieListPage> {
        return getMovieListPageByGenreUseCase.execute(genreId, page)
    }

    override suspend fun getMovieDetail(movieId: Int): Resource<MovieDetail> {
        return getMovieDetailUseCase.execute(movieId)
    }

    fun setGenreId(value: Int) {
        genreId = value
    }

}