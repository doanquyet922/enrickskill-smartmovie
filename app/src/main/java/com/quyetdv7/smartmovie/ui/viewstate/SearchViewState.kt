package com.quyetdv7.smartmovie.ui.viewstate

import com.quyetdv7.domain.model.detail.Genre
import com.quyetdv7.domain.model.list.MovieListPage

data class SearchViewState(
    val currentQuery: String,
    val genreList: List<Genre>,
    val currentPage: Int,
    val isLoadingMore: Boolean,
    val isLoading: Boolean,
    val isError: Boolean,
    val moviePages: MutableList<MovieListPage>
)