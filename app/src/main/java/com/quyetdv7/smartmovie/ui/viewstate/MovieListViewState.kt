package com.quyetdv7.smartmovie.ui.viewstate

import com.quyetdv7.domain.model.favorite.FavoriteMovie
import com.quyetdv7.domain.model.list.MovieListPage

data class MovieListViewState(
    val movieListPages: MutableList<MovieListPage>,
    val favList: List<FavoriteMovie>,
    val currentPage: Int,
    val isLoading: Boolean,
    val isLoadingMore: Boolean,
    val isError: Boolean
)