package com.quyetdv7.smartmovie.ui.viewmodel.home

import com.quyetdv7.domain.Resource
import com.quyetdv7.domain.model.detail.MovieDetail
import com.quyetdv7.domain.model.list.MovieListPage
import com.quyetdv7.domain.usecase.GetMovieDetailUseCase
import com.quyetdv7.domain.usecase.GetUpComingMovieListPageUseCase
import com.quyetdv7.smartmovie.ui.base.viewmodel.BaseMovieListViewModel
import javax.inject.Inject

class UpComingListViewModel @Inject constructor(
    private val getUpComingMovieListPageUseCase: GetUpComingMovieListPageUseCase,
    private val getMovieDetailUseCase: GetMovieDetailUseCase
) : BaseMovieListViewModel() {

    override suspend fun getMovieListPage(page: Int): Resource<MovieListPage> {
        return getUpComingMovieListPageUseCase.execute(page)
    }

    override suspend fun getMovieDetail(movieId: Int): Resource<MovieDetail> {
        return getMovieDetailUseCase.execute(movieId)
    }

}