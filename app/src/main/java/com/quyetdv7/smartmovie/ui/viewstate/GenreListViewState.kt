package com.quyetdv7.smartmovie.ui.viewstate

import com.quyetdv7.domain.model.detail.Genre

data class GenreListViewState(
    val genres: List<Genre>,
    val isError: Boolean,
    val isLoading: Boolean
)