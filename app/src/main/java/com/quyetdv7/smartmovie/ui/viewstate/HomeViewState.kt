package com.quyetdv7.smartmovie.ui.viewstate

import kotlinx.coroutines.flow.Flow
import com.quyetdv7.domain.Resource
import com.quyetdv7.domain.model.favorite.FavoriteMovie
import com.quyetdv7.domain.model.list.MovieListPage
import com.quyetdv7.smartmovie.utils.MovieCategory
import com.quyetdv7.smartmovie.utils.MovieItemDisplayType

data class HomeViewState(
    val isLoading: Boolean,
    val isError: Boolean,
    val currentPageType: MovieCategory?,
    val currentDisplayType: MovieItemDisplayType,
    val favoriteList: Flow<List<FavoriteMovie>>?,
    val movieSectionMap: MutableMap<MovieCategory, Resource<MovieListPage>?>,
)