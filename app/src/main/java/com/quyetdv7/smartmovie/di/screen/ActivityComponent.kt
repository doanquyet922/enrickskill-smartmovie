package com.quyetdv7.smartmovie.di.screen

import dagger.Subcomponent
import com.quyetdv7.smartmovie.di.scope.PerScreen
import com.quyetdv7.smartmovie.ui.view.MainActivity

@PerScreen
@Subcomponent(modules = [ActivityModule::class])
interface ActivityComponent {
    fun inject(activity: MainActivity)
}