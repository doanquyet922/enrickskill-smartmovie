package com.quyetdv7.smartmovie.di.screen

import dagger.Module
import dagger.Provides
import com.quyetdv7.smartmovie.di.scope.PerFragment
import com.quyetdv7.smartmovie.ui.base.fragment.BaseFragment

@Module
class FragmentModule(private val fragment: BaseFragment) {
    @PerFragment
    @Provides
    fun provideFragment(): BaseFragment {
        return fragment
    }
}