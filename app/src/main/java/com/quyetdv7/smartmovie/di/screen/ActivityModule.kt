package com.quyetdv7.smartmovie.di.screen

import dagger.Module
import dagger.Provides
import com.quyetdv7.smartmovie.di.scope.PerScreen
import com.quyetdv7.smartmovie.ui.base.activity.BaseActivity

@Module
class ActivityModule(private val activity: BaseActivity) {
    @PerScreen
    @Provides
    fun provideActivity(): BaseActivity {
        return activity
    }
}

