package com.quyetdv7.smartmovie.di.application

import dagger.Module
import dagger.Provides
import com.quyetdv7.data.mapper.*
import com.quyetdv7.data.repository.FavoriteMovieRepositoryImpl
import com.quyetdv7.data.repository.GenreRepositoryImpl
import com.quyetdv7.data.repository.MovieRepositoryImpl
import com.quyetdv7.data.repository.SearchRepositoryImpl
import com.quyetdv7.data.source.local.FavoriteMovieDatabase
import com.quyetdv7.data.source.remote.DiscoverApi
import com.quyetdv7.data.source.remote.GenreApi
import com.quyetdv7.data.source.remote.MovieApi
import com.quyetdv7.data.source.remote.SearchApi
import com.quyetdv7.data.utils.DispatcherProvider
import com.quyetdv7.domain.repository.FavoriteMovieRepository
import com.quyetdv7.domain.repository.GenreRepository
import com.quyetdv7.domain.repository.MovieRepository
import com.quyetdv7.domain.repository.SearchRepository
import com.quyetdv7.smartmovie.SmartMovieApplication
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Singleton
    @Provides
    fun provideMovieRepository(
        movieApi: MovieApi,
        dispatcherProvider: DispatcherProvider,
        movieDetailMapper: MovieDetailMapper,
        movieListPageMapper: MovieListPageMapper,
        castAndCrewMapper: CastAndCrewMapper
    ): MovieRepository = MovieRepositoryImpl(
        movieApi,
        dispatcherProvider,
        movieDetailMapper,
        movieListPageMapper,
        castAndCrewMapper
    )


    @Singleton
    @Provides
    fun provideGenreRepository(
        genreApi: GenreApi,
        discoverApi: DiscoverApi,
        dispatcherProvider: DispatcherProvider,
        genreMapper: GenreMapper,
        movieListPageMapper: MovieListPageMapper
    ): GenreRepository = GenreRepositoryImpl(
        genreApi,
        discoverApi,
        dispatcherProvider,
        genreMapper,
        movieListPageMapper
    )


    @Singleton
    @Provides
    fun provideSearchRepository(
        searchApi: SearchApi,
        dispatcherProvider: DispatcherProvider,
        movieListPageMapper: MovieListPageMapper
    ): SearchRepository = SearchRepositoryImpl(searchApi, dispatcherProvider, movieListPageMapper)


    @Singleton
    @Provides
    fun provideFavoriteMovieRepository(
        context: SmartMovieApplication,
        dispatcherProvider: DispatcherProvider,
        favoriteMovieMapper: FavoriteMovieMapper
    ): FavoriteMovieRepository = FavoriteMovieRepositoryImpl(
        FavoriteMovieDatabase.getInstance(context).favoriteMovieDao(),
        dispatcherProvider,
        favoriteMovieMapper
    )
}