package com.quyetdv7.smartmovie.di.application

import dagger.Component
import com.quyetdv7.smartmovie.SmartMovieApplication
import com.quyetdv7.smartmovie.di.screen.ActivityComponent
import com.quyetdv7.smartmovie.di.screen.ActivityModule
import com.quyetdv7.smartmovie.di.screen.FragmentComponent
import com.quyetdv7.smartmovie.di.screen.FragmentModule
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, ApiModule::class, RepositoryModule::class, ViewModelModule::class])
interface ApplicationComponent {

    fun inject(application: SmartMovieApplication)

    fun plus(screenModule: ActivityModule): ActivityComponent

    fun plus(fragmentModule: FragmentModule): FragmentComponent
}