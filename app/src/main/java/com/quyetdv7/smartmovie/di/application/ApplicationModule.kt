package com.quyetdv7.smartmovie.di.application

import dagger.Module
import dagger.Provides
import com.quyetdv7.data.utils.DispatcherProvider
import com.quyetdv7.data.utils.StandardDispatchers
import com.quyetdv7.smartmovie.SmartMovieApplication
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: SmartMovieApplication) {

    @Provides
    @Singleton
    fun provideApplication(): SmartMovieApplication {
        return application
    }

    @Singleton
    @Provides
    fun provideDispatcherProvider(): DispatcherProvider = StandardDispatchers()

}