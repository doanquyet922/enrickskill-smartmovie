package com.quyetdv7.smartmovie.di.application

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import com.quyetdv7.smartmovie.di.scope.ViewModelKey
import com.quyetdv7.smartmovie.ui.base.viewmodel.ViewModelFactory
import com.quyetdv7.smartmovie.ui.viewmodel.home.HomeViewModel

@Module
abstract class ViewModelModule {
    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindHomeViewModel(homeViewModel: HomeViewModel): ViewModel
}