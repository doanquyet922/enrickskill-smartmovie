package com.quyetdv7.smartmovie.di.screen

import com.quyetdv7.smartmovie.ui.view.fragments.genre.GenreFragment
import com.quyetdv7.smartmovie.ui.view.fragments.moviebygenre.MovieByGenreFragment
import com.quyetdv7.smartmovie.ui.view.fragments.moviedetail.MovieDetailFragment
import com.quyetdv7.smartmovie.ui.view.fragments.search.SearchFragment
import dagger.Subcomponent
import com.quyetdv7.smartmovie.di.scope.PerFragment
import com.quyetdv7.smartmovie.ui.view.fragments.artist.ArtistFragment
import com.quyetdv7.smartmovie.ui.view.fragments.home.*

@PerFragment
@Subcomponent(modules = [FragmentModule::class])
interface FragmentComponent {
    fun inject(fragment: HomeFragment)
    fun inject(fragment: AllMovieFragment)
    fun inject(fragment: NowPlayingBaseMovieFragment)
    fun inject(fragment: PopularBaseMovieFragment)
    fun inject(fragment: TopRatedBaseMovieFragment)
    fun inject(fragment: UpComingBaseMovieFragment)
    fun inject(fragment: SearchFragment)
    fun inject(fragment: ArtistFragment)
    fun inject(fragment: GenreFragment)
    fun inject(fragment: MovieDetailFragment)
    fun inject(fragment: MovieByGenreFragment)
}
